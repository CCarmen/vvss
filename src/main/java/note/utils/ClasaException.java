package note.utils;

/**
 * @author carmen.cirebea
 * @since 3/5/2018
 */
public class ClasaException extends Throwable {
    private long serialVersionUID;

    public ClasaException(String message) {
        super(message);
    }
}
