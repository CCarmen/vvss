package note.model;

public class Nota {
	
	private double nrmatricol;
	private String materie;
	private double nota;
	
	public Nota(double nrmatricol, String materie, double nota) {
		this.setNrmatricol(nrmatricol);
		this.setMaterie(materie);
		this.setNota(nota);
	}

	public double getNrmatricol() {
		return nrmatricol;
	}

	public void setNrmatricol(double nrmatricol) {
		this.nrmatricol = nrmatricol;
	}

	public String getMaterie() {
		return materie;
	}

	public void setMaterie(String materie) {
		this.materie = materie;
	}

	public double getNota() {
		return nota;
	}

	public void setNota(double nota) {
		this.nota = nota;
	}
	
	
}
