package note.model;

public class Corigent {
	private String numeElev;
	private int nrMaterii;
	
	public Corigent(String numeElev, int nrMaterii) {
		this.numeElev = numeElev;
		this.nrMaterii = nrMaterii;
	}
	
	
	public String getNumeElev() {
		return numeElev;
	}

	public void setNumeElev(String numeElev) {
		this.numeElev = numeElev;
	}

	public int getNrMaterii() {
		return nrMaterii;
	}

	public void setNrMaterii(int nrMaterii) {
		this.nrMaterii = nrMaterii;
	}

	public String toString() {
		return numeElev + " -> " + nrMaterii;
	}
}
