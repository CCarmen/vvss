package note.model;

public class Medie {
	private Elev elev;
	private double medie;

	public Elev getElev() {
		return elev;
	}

	public void setElev(Elev elev) {
		this.elev = elev;
	}

	public double getMedie() {
		return medie;
	}

	public void setMedie(double medie) {
		this.medie = medie;
	}
	
	public String toString() {
		return this.elev.getNume() + " -> " + this.medie;
	}
	
	
}
