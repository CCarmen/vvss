package note;

import note.model.Corigent;
import note.model.Nota;
import note.repository.ClasaRepository;
import note.repository.ClasaRepositoryMock;
import note.repository.NoteRepository;
import note.repository.NoteRepositoryMock;
import note.utils.ClasaException;
import note.utils.Constants;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class TopDownTest {
    private ClasaRepository clasaRepository;
    private NoteRepository noteRepository;

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Before
    public void init() {
        clasaRepository = new ClasaRepositoryMock();
        noteRepository = new NoteRepositoryMock();
    }

    @Test
    public void unitTestFirstRequest() throws ClasaException {
        Nota added = new Nota(1, "romana", 8.9);
        noteRepository.addNota(added);
        List<Nota> note = noteRepository.getNote();
        assertEquals(note.get(note.size() - 1).getMaterie(), added.getMaterie());
        assert (note.get(note.size() - 1).getNota() == added.getNota());
        assert (note.get(note.size() - 1).getNrmatricol() == added.getNrmatricol());
    }

    @Test
    public void integrationSecondRequest() throws ClasaException {
        //cerinta i
        expectedEx.expect(ClasaException.class);
        expectedEx.expectMessage(Constants.invalidNota);
        noteRepository.addNota(new Nota(1,"romana",0));
        //cerinta ii
        expectedEx.expect(ClasaException.class);
        expectedEx.expectMessage(Constants.emptyRepository);
        clasaRepository.calculeazaMedii();
    }

    @Test
    public void integrationThirdRequest() throws ClasaException {
        //cerinta i
        expectedEx.expect(ClasaException.class);
        expectedEx.expectMessage(Constants.invalidMateria);
        noteRepository.addNota(new Nota(1,"",10));
        //cerinta ii
        expectedEx.expect(ClasaException.class);
        expectedEx.expectMessage(Constants.emptyRepository);
        clasaRepository.calculeazaMedii();
        //cerinta iii
        List<Corigent> corigenti = clasaRepository.getCorigenti();
        assert(corigenti.size() == 0);
    }
}
