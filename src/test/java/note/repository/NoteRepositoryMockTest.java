package note.repository;

import note.model.Nota;
import note.utils.ClasaException;
import note.utils.Constants;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.List;

import static org.junit.Assert.*;

public class NoteRepositoryMockTest {

    private NoteRepository noteRepository;

    public NoteRepositoryMockTest(){}

    @Before
    public void init() {
        noteRepository = new NoteRepositoryMock();
    }

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void testAddNotaAllGood() throws ClasaException {
        Nota added = new Nota(1, "romana", 8.9);
        noteRepository.addNota(added);
        List<Nota> note = noteRepository.getNote();
        assertEquals(note.get(note.size() - 1).getMaterie(), added.getMaterie());
        assert (note.get(note.size() - 1).getNota() == added.getNota());
        assert (note.get(note.size() - 1).getNrmatricol() == added.getNrmatricol());
    }

    @Test
    public void testAddNotaMaterieEmptyString() throws ClasaException {
        expectedEx.expect(ClasaException.class);
        expectedEx.expectMessage(Constants.invalidMateria);
        noteRepository.addNota(new Nota(1,"",10));
    }

    @Test
    public void testAddNotaNrMatricolNotInt() throws ClasaException {
        expectedEx.expect(ClasaException.class);
        expectedEx.expectMessage(Constants.invalidNrmatricol);
        noteRepository.addNota(new Nota(1.5,"romana",10));
    }

    @Test
    public void testAddNotaNrMatricolLess() throws ClasaException {
        expectedEx.expect(ClasaException.class);
        expectedEx.expectMessage(Constants.invalidNrmatricol);
        noteRepository.addNota(new Nota(0,"romana",10));
    }

    @Test
    public void testAddNotaNrMatricolMore() throws ClasaException {
        expectedEx.expect(ClasaException.class);
        expectedEx.expectMessage(Constants.invalidNrmatricol);
        noteRepository.addNota(new Nota(1001,"romana",10));
    }

    @Test
    public void testAddNotaLess() throws ClasaException {
        expectedEx.expect(ClasaException.class);
        expectedEx.expectMessage(Constants.invalidNota);
        noteRepository.addNota(new Nota(1,"romana",0));
    }

    @Test
    public void testAddNotaMore() throws ClasaException {
        expectedEx.expect(ClasaException.class);
        expectedEx.expectMessage(Constants.invalidNota);
        noteRepository.addNota(new Nota(1,"romana",11));
    }
}