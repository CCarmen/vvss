package note.repository;

import note.model.Corigent;
import note.model.Elev;
import note.model.Nota;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class CorigentiTest {

    private ClasaRepository clasaRepository;

    @Before
    public void init() {
        clasaRepository = new ClasaRepositoryMock();
    }

    @Test
    public void noDataTest() {
        List<Corigent> corigenti = clasaRepository.getCorigenti();
        assert(corigenti.size() == 0);
    }

    @Test
    public void getCorigentiTest() {
        List<Elev> elevi = new ArrayList<Elev>();
        Elev ion = new Elev(1,"ion");
        elevi.add(ion);
        Elev gheorghe = new Elev(2,"gheorghe");
        elevi.add(gheorghe);
        elevi.add(new Elev(3,"Maria"));
        List<Nota> note = new ArrayList<Nota>();
        note.add(new Nota(3,"romana",10));
        clasaRepository.creazaClasa(elevi,note);

        List<Corigent> corigenti = clasaRepository.getCorigenti();
        assert (corigenti.get(0).getNumeElev().equals("gheorghe"));
        assert (corigenti.get(1).getNumeElev().equals("ion"));
        assert (corigenti.get(0).getNrMaterii() == corigenti.get(1).getNrMaterii());
    }
}
