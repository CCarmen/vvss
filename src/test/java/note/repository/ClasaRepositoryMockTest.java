package note.repository;

import note.model.Elev;
import note.model.Medie;
import note.model.Nota;
import note.utils.ClasaException;
import note.utils.Constants;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import static org.junit.Assert.*;


import java.util.ArrayList;
import java.util.List;

public class ClasaRepositoryMockTest {


    private ClasaRepository clasaRepository;

    public ClasaRepositoryMockTest(){}

    @Before
    public void init() {
        clasaRepository = new ClasaRepositoryMock();
    }

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void noDataExceptionTest() throws ClasaException{
        expectedEx.expect(ClasaException.class);
        expectedEx.expectMessage(Constants.emptyRepository);
        clasaRepository.calculeazaMedii();
    }

    @Test
    public void noGradesTest() throws ClasaException {
        List<Elev> elevi = new ArrayList<Elev>();
        Elev ion = new Elev(1,"ion");
        elevi.add(ion);
        elevi.add(new Elev(2,"maria"));
        List<Nota> note = new ArrayList<Nota>();
        note.add(new Nota(2,"romana",10));
        clasaRepository.creazaClasa(elevi,note);
        List<Medie> medii = clasaRepository.calculeazaMedii();
        assertEquals(medii.get(1).getElev().getNume(),"ion");
        assert(medii.get(1).getMedie() == 0.0);
    }

    @Test
    public void moreGradesTest() throws ClasaException {
        List<Elev> elevi = new ArrayList<Elev>();
        Elev ion = new Elev(1,"ion");
        elevi.add(ion);
        List<Nota> note = new ArrayList<Nota>();
        note.add(new Nota(1,"romana",10));
        note.add(new Nota(1,"romana",8));
        clasaRepository.creazaClasa(elevi,note);
        List<Medie> medii = clasaRepository.calculeazaMedii();
        assertTrue(medii.get(0).getMedie()==9.0);
    }
}
