package note;

import note.model.Corigent;
import note.model.Elev;
import note.model.Medie;
import note.model.Nota;
import note.repository.ClasaRepository;
import note.repository.ClasaRepositoryMock;
import note.repository.NoteRepositoryMock;
import note.repository.NoteRepository;
import note.utils.ClasaException;
import note.utils.Constants;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class BigBangTest {

    private ClasaRepository clasaRepository;
    private NoteRepository noteRepository;

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Before
    public void init() {
        clasaRepository = new ClasaRepositoryMock();
        noteRepository = new NoteRepositoryMock();
    }

    @Test
    public void unitTestFirstRequest() throws ClasaException {
        Nota added = new Nota(1, "romana", 8.9);
        noteRepository.addNota(added);
        List<Nota> note = noteRepository.getNote();
        assertEquals(note.get(note.size() - 1).getMaterie(), added.getMaterie());
        assert (note.get(note.size() - 1).getNota() == added.getNota());
        assert (note.get(note.size() - 1).getNrmatricol() == added.getNrmatricol());
    }

    @Test
    public void unitTestSecondRequest() throws ClasaException {
        List<Elev> elevi = new ArrayList<Elev>();
        Elev ion = new Elev(1,"ion");
        elevi.add(ion);
        List<Nota> note = new ArrayList<Nota>();
        note.add(new Nota(1,"romana",10));
        note.add(new Nota(1,"romana",8));
        clasaRepository.creazaClasa(elevi,note);
        List<Medie> medii = clasaRepository.calculeazaMedii();
        assertTrue(medii.get(0).getMedie()==9.0);
    }

    @Test
    public void unitTestThirdRequest() {
        List<Elev> elevi = new ArrayList<Elev>();
        Elev ion = new Elev(1,"ion");
        elevi.add(ion);
        Elev gheorghe = new Elev(2,"gheorghe");
        elevi.add(gheorghe);
        elevi.add(new Elev(3,"Maria"));
        List<Nota> note = new ArrayList<Nota>();
        note.add(new Nota(3,"romana",10));
        clasaRepository.creazaClasa(elevi,note);

        List<Corigent> corigenti = clasaRepository.getCorigenti();
        assert (corigenti.get(0).getNumeElev().equals("gheorghe"));
        assert (corigenti.get(1).getNumeElev().equals("ion"));
        assert (corigenti.get(0).getNrMaterii() == corigenti.get(1).getNrMaterii());
    }

    @Test
    public void integrationTest() throws ClasaException{
        //cerinta i
        expectedEx.expect(ClasaException.class);
        expectedEx.expectMessage(Constants.invalidNota);
        noteRepository.addNota(new Nota(1,"romana",0));
        //cerinta ii
        expectedEx.expect(ClasaException.class);
        expectedEx.expectMessage(Constants.emptyRepository);
        clasaRepository.calculeazaMedii();
        //cerinta iii
        List<Corigent> corigenti = clasaRepository.getCorigenti();
        assert(corigenti.size() == 0);
    }
}
